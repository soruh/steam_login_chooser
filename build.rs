use std::io::Write;

use std::path::PathBuf;

fn main() {
    let icon = PathBuf::from(std::env::var("CARGO_MANIFEST_DIR").unwrap()).join("login.ico");

    let image = image::open(&icon).unwrap();

    assert!(
        image.width() % 4 == 0 && image.height() % 4 == 0,
        "icon dimensions must be a multiple of 4"
    );

    let icon_data_path = PathBuf::from(std::env::var("OUT_DIR").unwrap()).join("icon.rgba");

    let mut file = std::fs::File::create(icon_data_path).unwrap();

    file.write_all(&image.width().to_le_bytes()).unwrap();
    file.write_all(&image.height().to_le_bytes()).unwrap();
    file.write_all(image.as_rgba8().unwrap()).unwrap();

    if std::env::var("CARGO_CFG_TARGET_OS").unwrap() == "windows" {
        let mut res = winres::WindowsResource::new();

        #[cfg(target_family = "unix")]
        {
            let windres = match std::env::var("CARGO_CFG_TARGET_POINTER_WIDTH")
                .unwrap()
                .as_str()
            {
                "64" => "x86_64-w64-mingw32-windres",
                "32" => "i686-w64-mingw32-windres",
                width => panic!("unexpected pointer width: {}", width),
            };

            res.set_windres_path(
                which::which(windres)
                    .unwrap_or_else(|err| {
                        panic!("could not find windres binary {:?}: {}", windres, err)
                    })
                    .to_str()
                    .unwrap(),
            );
        }

        res.set_icon(icon.to_str().unwrap());
        res.compile().expect("failed to build executable logo.");
    }
}
