use std::{path::PathBuf, process::Command, thread, time::Duration};

#[cfg(target_family = "windows")]
mod arch {
    pub use extract_macro::extract;
    pub use std::os::windows::process::CommandExt;

    pub const DETACHED_PROCESS: u32 = 0x00000008;
    pub const CREATE_NO_WINDOW: u32 = 0x08000000;
}

#[cfg(not(target_family = "windows"))]
mod arch {

    pub use std::collections::HashMap;
    pub use std::io::{BufReader, BufWriter, Seek, SeekFrom};
    pub use vdf::{Value, Writer};

    pub fn get_path<'r>(
        mut registry: &'r mut HashMap<String, Value>,
        path: &[&str],
    ) -> &'r mut HashMap<String, Value> {
        for segment in path {
            let val = registry.get_mut(*segment).unwrap_or_else(|| {
                panic!(
                    "registry.vdf is missing path segment {:?} of path {:?}",
                    segment, path
                )
            });

            if let vdf::Value::Dict(dict) = val {
                registry = dict;
            } else {
                panic!(
                    "Path segment {:?} of path {:?} is not a dictionary",
                    segment, path
                );
            }
        }

        registry
    }
}

use arch::*;

pub fn find_steam_paths() -> Vec<PathBuf> {
    #[cfg(target_family = "unix")]
    {
        standard_paths::StandardPaths::new("", "")
            .locate_all(
                standard_paths::LocationType::GenericDataLocation,
                "Steam",
                standard_paths::LocateOption::LocateDirectory,
            )
            .unwrap()
            .expect("Could not find steam installation folder")
    }

    #[cfg(target_family = "windows")]
    [
        PathBuf::from("C:\\Program Files (x86)\\Steam"),
        PathBuf::from("C:\\Program Files\\Steam"),
    ]
    .into_iter()
    .filter(|path| path.exists())
    .collect()
}
pub fn set_steam_registry(auto_login_user: &str, remember_password: bool) {
    #[cfg(target_family = "windows")]
    {
        use registry::*;
        let regkey = Hive::CurrentUser
            .open(r"SOFTWARE\Valve\Steam", Security::Write)
            .expect("Could not open steam registry");
        regkey
            .set_value(
                "AutoLoginUser",
                &Data::String(
                    auto_login_user
                        .try_into()
                        .expect("Failed to write user name to registry"),
                ),
            )
            .expect("Failed to set value in steam registry");
        regkey
            .set_value("RememberPassword", &Data::U32(remember_password as u32))
            .expect("Failed to set value in steam registry");
    }

    #[cfg(target_family = "unix")]
    {
        let steam = standard_paths::StandardPaths::new("", "")
            .locate(
                standard_paths::LocationType::HomeLocation,
                ".steam",
                standard_paths::LocateOption::LocateDirectory,
            )
            .expect("Failed to locate steam config folder")
            .expect("Failed to locate steam config folder");

        let registry_path = steam.join("registry.vdf");

        let mut file = std::fs::OpenOptions::new()
            .read(true)
            .write(true)
            .create(false)
            .open(&registry_path)
            .expect("Failed to open steam registry for writing");

        let mut registry = vdf::from_reader(BufReader::new(&mut file), "Registry")
            .expect("Failed to read steam registry");

        let steam = get_path(&mut registry, &["HKCU", "Software", "Valve", "Steam"]);

        steam.insert(
            "AutoLoginUser".to_owned(),
            Value::String(auto_login_user.to_owned()),
        );
        steam.insert(
            "RememberPassword".to_owned(),
            Value::String((remember_password as u32).to_string()),
        );

        file.set_len(0)
            .expect("Failed to truncate steam registry.vdf");
        file.seek(SeekFrom::Start(0)).unwrap();

        Writer::new(true, &mut BufWriter::new(&mut file))
            .write("Registry", &registry)
            .expect("Failed to write to steam registry");
    }
}

pub fn get_steam_registry() -> (String, bool) {
    #[cfg(target_family = "windows")]
    {
        use registry::*;
        let regkey = Hive::CurrentUser
            .open(r"SOFTWARE\Valve\Steam", Security::Read)
            .expect("Could not open steam registry");

        (
            extract!(
                Data::String(x),
                x.to_string().unwrap(),
                regkey
                    .value("AutoLoginUser")
                    .expect("Failed to get value in steam registry"),
            )
            .expect("registry value had invalid type"),
            extract!(
                Data::U32(x),
                x != 0,
                regkey
                    .value("RememberPassword")
                    .expect("Failed to get value in steam registry")
            )
            .expect("registry value had invalid type"),
        )
    }

    #[cfg(target_family = "unix")]
    {
        let steam = standard_paths::StandardPaths::new("", "")
            .locate(
                standard_paths::LocationType::HomeLocation,
                ".steam",
                standard_paths::LocateOption::LocateDirectory,
            )
            .expect("Failed to locate steam config folder")
            .expect("Failed to locate steam config folder");

        let registry_path = steam.join("registry.vdf");

        let file = std::fs::OpenOptions::new()
            .read(true)
            .write(true)
            .create(false)
            .open(&registry_path)
            .expect("Failed to open steam registry for writing");

        let mut registry = vdf::from_reader(BufReader::new(file), "Registry")
            .expect("Failed to read steam registry");

        let steam = get_path(&mut registry, &["HKCU", "Software", "Valve", "Steam"]);

        (
            steam.remove("AutoLoginUser").unwrap().as_string().unwrap(),
            steam
                .remove("RememberPassword")
                .unwrap()
                .as_string()
                .unwrap()
                .parse::<u32>()
                .unwrap()
                != 0,
        )
    }
}

#[derive(Debug)]
pub struct User {
    pub persona_name: String,
    pub account_name: String,
    pub remember_password: bool,
    pub most_recent: bool,
    pub timestamp: i64,
}

pub fn steam_pids() -> Vec<u32> {
    #[cfg(target_family = "unix")]
    {
        std::str::from_utf8(
            &Command::new("ps")
                .arg("-C")
                .arg("steam")
                .output()
                .expect("Failed to run ps")
                .stdout,
        )
        .unwrap()
        .lines()
        .skip(1)
        .map(|x| x.trim_start().split(' ').next().unwrap().parse().unwrap())
        .collect()
    }

    #[cfg(target_family = "windows")]
    {
        std::str::from_utf8(
            &Command::new("tasklist")
                .creation_flags(CREATE_NO_WINDOW)
                .arg("/nh")
                .arg("/fo")
                .arg("csv")
                .arg("/fi")
                .arg("IMAGENAME eq steam.exe")
                .output()
                .expect("Failed to run ps")
                .stdout,
        )
        .unwrap()
        .lines()
        .filter_map(|x| {
            x.split(',')
                .nth(1)?
                .strip_prefix('"')?
                .strip_suffix('"')?
                .parse()
                .ok()
        })
        .collect()
    }
}

pub fn get_users() -> Vec<User> {
    let steam_path = find_steam_paths()
        .into_iter()
        .find(|path| path.join("config").join("loginusers.vdf").exists())
        .expect("Could not find steam installation folder");

    let config_folder = steam_path.join("config");

    let loginusers = vdf::from_file(config_folder.join("loginusers.vdf"), "users").unwrap();

    macro_rules! field {
        ($dict: ident, $key: literal, String) => {
            $dict
                .remove($key)
                .unwrap_or_else(|| panic!("Missing key {}", $key))
                .as_string()
                .unwrap_or_else(|| panic!("Key {} wasn't a string", $key))
        };
        ($dict: ident, $key: literal, i64) => {
            field!($dict, $key, String)
                .parse::<i64>()
                .unwrap_or_else(|err| panic!("Key {} wasn't an integer: {err}", $key))
        };
        ($dict: ident, $key: literal, bool) => {
            field!($dict, $key, i64) != 0
        };
    }

    let mut users: Vec<_> = loginusers
        .into_values()
        .map(|val| {
            let mut dict = val
                .as_dict()
                .expect("loginuser.vdf value was not a dictionary");

            User {
                persona_name: field!(dict, "PersonaName", String),
                account_name: field!(dict, "AccountName", String),
                remember_password: field!(dict, "RememberPassword", bool),
                most_recent: field!(dict, "MostRecent", bool),
                timestamp: field!(dict, "Timestamp", i64),
            }
        })
        .collect();

    users.sort_unstable_by_key(|u| u.timestamp);
    users.reverse();

    users
}

pub fn kill_steam() {
    let pids = steam_pids();

    if !pids.is_empty() {
        #[cfg(target_family = "unix")]
        {
            let mut ps = Command::new("kill");
            let mut ps = &mut ps;

            for pid in &pids {
                ps = ps.arg(&format!("{}", pid));
            }

            assert!(
                ps.status().expect("Failed to kill steam").success(),
                "Failed to kill steam"
            );
        }

        #[cfg(target_family = "windows")]
        {
            assert!(
                Command::new("taskkill")
                    .creation_flags(CREATE_NO_WINDOW)
                    .arg("/f")
                    .arg("/im")
                    .arg("steam.exe")
                    .status()
                    .expect("Failed to kill steam")
                    .success(),
                "Failed to kill steam"
            );
        }

        let mut new_pids = steam_pids();

        while {
            let mut all_dead = false;
            for pid in &pids {
                if new_pids.contains(pid) {
                    all_dead = true;
                    break;
                }
            }
            all_dead
        } {
            thread::sleep(Duration::from_millis(100));
            new_pids = steam_pids();
        }
    }
}

pub fn start_steam() {
    #[cfg(target_family = "unix")]
    {
        Command::new("steam")
            .spawn()
            .expect("Failed to spawn steam");
    }

    #[cfg(target_family = "windows")]
    {
        let steam = find_steam_paths()
            .into_iter()
            .find(|path| path.join("steam.exe").exists())
            .expect("Could not find steam installation folder")
            .join("steam.exe");

        Command::new(steam)
            .creation_flags(DETACHED_PROCESS)
            .spawn()
            .expect("Failed to spawn steam");
    }

    while steam_pids().is_empty() {
        thread::sleep(Duration::from_millis(100));
    }
}
