use std::sync::{mpsc::Sender, Arc};

use chrono::{offset::Local, TimeZone};
use eframe::{
    egui::{self, mutex::Mutex, panel::TopBottomSide, CentralPanel, Grid, TopBottomPanel},
    epi::{self, Frame},
};

use crate::steam::{get_steam_registry, get_users, kill_steam, set_steam_registry, start_steam};

pub struct App {
    tx: Option<Sender<Frame>>,
    state: Arc<Mutex<Vec<u32>>>,
}

impl App {
    pub fn new(tx: Sender<Frame>, state: Arc<Mutex<Vec<u32>>>) -> Self {
        Self {
            state,
            tx: Some(tx),
        }
    }
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Steam Login Chooser"
    }

    /// Called once before the first frame.
    fn setup(&mut self, _ctx: &egui::CtxRef, frame: &Frame, _storage: Option<&dyn epi::Storage>) {
        self.tx.take().unwrap().send(frame.clone()).unwrap();
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &epi::Frame) {
        let steam_pids = self.state.lock();

        TopBottomPanel::new(TopBottomSide::Top, "top_panel").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.label(format!(
                    "steam is {}running",
                    if steam_pids.is_empty() { "not " } else { "" }
                ));

                if steam_pids.is_empty() {
                    if ui.button("start steam").clicked() {
                        start_steam();
                    }
                } else {
                    if ui.button("kill steam").clicked() {
                        kill_steam();
                    }
                    if ui.button("restart steam").clicked() {
                        kill_steam();
                        start_steam();
                    }
                }
            })
        });

        CentralPanel::default().show(ctx, |ui| {
            Grid::new("user_grid").num_columns(3).show(ui, |ui| {
                ui.label("account name");
                ui.label("account alias");
                ui.label("last used");
                //                ui.label("logged in");
                ui.end_row();
                let (selected, _) = get_steam_registry();
                for user in get_users() {
                    let selected = user.account_name == selected;

                    let a = ui.selectable_label(selected, &user.account_name);
                    let b = ui.selectable_label(selected, &user.persona_name);

                    if a.double_clicked() || b.double_clicked() {
                        set_steam_registry(&user.account_name, true);
                        kill_steam();
                        start_steam();
                    } else if a.clicked() || b.clicked() {
                        set_steam_registry(&user.account_name, true);
                    }

                    ui.label(Local.timestamp_opt(user.timestamp, 0).unwrap().to_string());
                    //                   ui.checkbox(&mut user.remember_password, "");
                    ui.end_row();
                }
            })
        });
    }
}
