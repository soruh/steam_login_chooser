#![windows_subsystem = "windows"]

use std::{
    sync::{mpsc::channel, Arc},
    thread::{sleep, spawn},
    time::{Duration, Instant},
};

use eframe::{egui::mutex::Mutex, epi::IconData};
use steam::steam_pids;

mod steam;
mod ui;

const ICON_DATA: &[u8] = include_bytes!(concat!(env!("OUT_DIR"), "/icon.rgba"));

fn main() {
    let (tx, rx) = channel();

    let state: Arc<Mutex<Vec<u32>>> = Default::default();
    let app = ui::App::new(tx, state.clone());

    let _ = spawn(move || {
        let frame = rx.recv().unwrap();
        loop {
            let start = Instant::now();

            *state.lock() = steam_pids();

            let next_update_in = Duration::from_millis(500);
            sleep(next_update_in.saturating_sub(start.elapsed()));
            frame.request_repaint();
        }
    });

    eframe::run_native(
        Box::new(app),
        eframe::NativeOptions {
            icon_data: Some(IconData {
                width: u32::from_le_bytes(ICON_DATA[..4].try_into().unwrap()),
                height: u32::from_le_bytes(ICON_DATA[4..8].try_into().unwrap()),
                rgba: ICON_DATA[8..].to_owned(),
            }),
            ..Default::default()
        },
    );
}
