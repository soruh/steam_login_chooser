# Steam Login Chooser

Allows selecting a steam account which will be used for steam auto login from the ones you have logged into before. (Usually you would need to start steam, logout, enter username, enter password). This allows you to just double click the account you want.

If your password is not saved by steam (you did not click "Remember my password" when logging in) you will need to enter it.
